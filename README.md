# Kubernetes learning Ingress

This repo houses two namespaces and works with Ingress

## Ingress controller

Why not multiple [ingress-controllers](https://kubernetes.github.io/ingress-nginx/user-guide/multiple-ingress/)?

## Getting started

Must create two namepaces  
- dev
- nginx
```
:~$ kubectl create ns dev
:~$ kubectl create ns nginx
```
Would also recommend deploying the metrics server:
[metrics server](https://github.com/kubernetes-sigs/metrics-server)

## YAML VIM

YAML, to help in setting up your YAML if you use vim I'm adding this section. If you want to paste into VIM you can use the `:set paste` option. Your *pasted* data will most likely be inserted with left adjusted margin. To move this (indent) to be properly formated you can issue the command `shift + v` for the VISUAL LINE editor and use the up/down arrows to highlight the lines you'd like to indent. Using `shift + >` you can indent (**this uses tab; YAML hates tab**). Once proper indention is set, use the `:retab` command to replace tab with whitespace character.  

The easy answer, use VSCode and ssh into your k8s controller.

Next is moving to the Nginx namepace directory [here](/ns_nginx)
